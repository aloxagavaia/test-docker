# Test task

## How to launch?

1. Clone the repository and get the necessary submodule: **git clone https://gitlab.com/aloxagavaia/test-docker.git**
2. Add the necessary submodule: **cd test-docker/application && git submodule init && git submodule update && cd ../**
3. Run composer "up" command: **docker-compose up -d**
4. Go inside the **app** container: **docker exec -it app bash**
5. Copy .env file: **cp .env.example .env**
6. Install composer dependencies: **composer install**
7. Install node dependencies and compile assets: **npm install && npm run dev** 
8. Run database migrations: **php artisan migrate --seed**
9. Run tests to check the working capacity: **php artisan test** or **php ./vendor/bin/phpunit**
10. Go to http://localhost